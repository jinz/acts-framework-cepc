add_executable(
  ACTFWHepMC3Example
  src/HepMC3Example.cpp)
target_link_libraries(
  ACTFWHepMC3Example
  PRIVATE ActsCore ACTFramework ACTFWPluginHepMC3 Boost::program_options)

install(TARGETS ACTFWHepMC3Example RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
install(FILES test.hepmc3 DESTINATION ${CMAKE_INSTALL_BINDIR})
